package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"live-gin/model"
	"net/http"
)

func GetUser(c *gin.Context) {
	username := c.Query("username")
	user, err := model.GetUserList(username)
	if err != nil {
		c.JSON(http.StatusForbidden, err)
	}
	c.JSON(http.StatusOK, user)
}
func RegisterUser(c *gin.Context) {
	password, _ := c.GetPostForm("password")
	username, _ := c.GetPostForm("username")
	fmt.Println(username, password, "username")
	user, err := model.Register(username, password)
	if err != nil {
		c.JSON(http.StatusServiceUnavailable, err.Error())
	}
	c.JSON(http.StatusOK, user)
}
