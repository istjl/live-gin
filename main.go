package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"live-gin/router"
)

func main() {

	var (
		err   error
		r, rr *gin.Engine
	)
	r = gin.Default()
	rr = router.GroupRouter(r)
	err = rr.Run(":8080")
	if err != nil {
		fmt.Println(err)
		return
	}
}
