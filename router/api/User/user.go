package User

import (
	"github.com/gin-gonic/gin"
	"live-gin/controller"
)

func Routers(r *gin.RouterGroup) {
	rr := r.Group("/user")
	rr.GET("/list", controller.GetUser)
	rr.POST("/register", controller.RegisterUser)
	return
}
