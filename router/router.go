package router

import (
	"github.com/gin-gonic/gin"
	"live-gin/router/api/User"
)

func GroupRouter(r *gin.Engine) *gin.Engine {
	rr := r.Group("/")
	User.Routers(rr)
	return r
}
