package model

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	DB  *gorm.DB
	err error
)

func init() {
	DB, err = gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%v)/%s?charset=utf8&parseTime=True&loc=Local", "eggapi", "19990218tjl.tjl", "114.67.77.90", "3306", "eggapi"))
	if err != nil {
		fmt.Println(err)
		return
	}
	DB.SingularTable(true)
	DB.LogMode(true)
}
