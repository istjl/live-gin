package model

import (
	"github.com/garyburd/redigo/redis"
	"live-gin/conf"
	"time"
)

var RedisClient *redis.Pool

func init() {
	RedisClient = &redis.Pool{
		MaxIdle:     16,
		MaxActive:   0,
		IdleTimeout: 300 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial(conf.RedisConf["type"], conf.RedisConf["address"])
			if err != nil {
				return nil, err
			}
			return c, nil
		},
	}
}
