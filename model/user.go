package model

import (
	"errors"
	"fmt"
	"time"
)

type User struct {
	Id           int    `json:"id"`
	Username     string `json:"user_name"`
	Password     string `json:"password"`
	Avatar       string `json:"avatar"`
	Coin         int    `json:"coin"`
	Created_time string `json:"create_time"`
	Updated_time string `json:"update_time"`
}

func (User) TblName() string {
	return "user"
}
func GetUserList(username string) (u []User, err error) {
	var (
		user   []User
		result interface{}
	)
	where := ""
	rc := RedisClient.Get()
	defer rc.Close()
	_, err = rc.Do("set", "test", "1")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(result)
	if username != "" {
		where += `username =  '` + username + "'"
	}
	err = DB.Where(where).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
func Register(username, password string) (u *User, err error) {
	if username == "" {
		return nil, errors.New("用户名不能为空")
	}
	if password == "" {
		return nil, errors.New("密码不能为空")
	}
	var (
		users []User
		user  User
	)
	user.Username = username
	err = DB.Where("username = ?", username).Find(&users).Error
	fmt.Println(users)
	if len(users) > 1 {
		return nil, errors.New("用户已存")
	}
	if err != nil {
		return nil, errors.New("创建用户失败")
	}
	user.Password = password
	user.Coin = 0
	user.Created_time = time.Now().Format("2006-01-02 15:04:05")
	user.Updated_time = time.Now().Format("2006-01-02 15:04:05")
	err = DB.Create(&user).Error
	if err != nil {
		return nil, errors.New("创建用户失败")
	}
	return &user, nil
}
