module live-gin

go 1.13

require (
	github.com/garyburd/redigo v1.6.2
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v8 v8.3.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/pilu/config v0.0.0-20131214182432-3eb99e6c0b9a // indirect
	github.com/pilu/fresh v0.0.0-20190826141211-0fa698148017 // indirect
	golang.org/x/sys v0.0.0-20201020230747-6e5568b54d1a // indirect
)
